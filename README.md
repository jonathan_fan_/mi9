### Quick summary ###
* This simple app filter out unwanted metadata of incoming json payload, and return expected records in json format via POST method.
* For GET method, only return 'It works!' in json format.

### Setup ###
* To setup, run 'npm install' in terminal 

### Content ###
* There are two modules 'app' and 'filter' with the name app.js and filter.js
* There is test.js in which all the tests are located. 

### Dependencies ###
* Using 'express' and 'body-parser' as production dependencies,
* Using 'mocha' and 'supertest' as dev dependencies.

### How to run this app ###
* Run  './bin/www' in terminal, make sure the user has permission to access port 80

## How to run tests ##
* Testing script is already included in package json, so use 'npm test' to run test in terminal.