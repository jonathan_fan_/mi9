/**
 * Author: Jonathan Fan
 */
var express    = require('express');
var bodyParser = require('body-parser');

var filter = express.Router();

filter.use(bodyParser.urlencoded({ extended: false }));
filter.use(bodyParser.json());

// Error Handling
filter.use (function (error, req, res, next){
    res.status(400).json({ 'error' : 'Could not decode request: JSON parsing failed'});
});

filter.route('/')
    .get(function(req, res){
        res.json({ message: 'It works!' });

    })
    .post(function(req, res) {
        res.json(filterPayload(req.body.payload))
    });


// FUNCTION
// =============================================================================
function filterPayload(payload) {
    var result = {};
    var valid_show_list = [];
    payload.forEach(function(show){

        if (show.drm && show.episodeCount > 0){
            valid_show_list.push({
                'image': show.image.showImage,
                'slug' : show.slug,
                'title': show.title
            })
        }
    });
    result.response = valid_show_list;
    return result;
}

module.exports = filter;
