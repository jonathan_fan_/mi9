/**
 * Author: Jonathan Fan
 */
var request = require('supertest');
var app = require('./app');

describe('Request to the root path', function() {

    it('Returns a 200 status code', function(done) {

        request(app)
            .get('/')
            .expect(200, done);
    });

    it('Return Json result', function (done) {

        var show = {
            "payload": [
                {
                    "country": "UK",
                    "description": "What's life like when you have enough children to field your own football team?",
                    "drm": true,
                    "episodeCount": 3,
                    "genre": "Reality",
                    "image": {
                        "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
                    },
                    "language": "English",
                    "nextEpisode": null,
                    "primaryColour": "#ff7800",
                    "seasons": [
                        {
                            "slug": "show/16kidsandcounting/season/1"
                        }
                    ],
                    "slug": "show/16kidsandcounting",
                    "title": "16 Kids and Counting",
                    "tvChannel": "GEM"
                }
            ],
            "skip": 0,
            "take": 10,
            "totalRecords": 1
        };

        var expectResult = {
            "response": [
                {
                    "image": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg",
                    "slug": "show/16kidsandcounting",
                    "title": "16 Kids and Counting"
                }
            ]
        };

        request(app)
            .post('/')
            .set('Accept', 'application/json')
            .send(show)
            .expect(200, expectResult, done)
    });

});