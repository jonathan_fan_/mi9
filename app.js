/**
 * Author: Jonathan Fan
 */

// BASE SETUP
var express = require('express');
var app = express();

var filter = require('./filter');
app.use('/', filter);

module.exports = app;
